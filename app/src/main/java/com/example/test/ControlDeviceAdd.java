package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ControlDeviceAdd extends AppCompatActivity {
    private String username ="";
    private Spinner spinner;
    private Spinner spinner2;
    private ArrayList<Sensor> sensorList = new ArrayList<Sensor>();
    private ArrayAdapter<Sensor> sensorListAdapter;
    private String[] deviceTypes = {"light", "water"};
    private String selectedType = "";
    private ArrayAdapter<String> deviceTypesAdapter;

    public void submitListener(View v) {
        String name = ((EditText)findViewById(R.id.textViewDeviceName)).getText().toString();
        String deviceId = ((EditText)findViewById(R.id.textViewDeviceId)).getText().toString();
        String sensorId = ((TextView)findViewById(R.id.textViewSensorID)).getText().toString();
        String sensorType = ((TextView)findViewById(R.id.textViewSensorType)).getText().toString();
        String lowestStats = ((EditText)findViewById(R.id.textViewSensorMin)).getText().toString();
        String highestStats = ((EditText)findViewById(R.id.textViewSensorMax)).getText().toString();
        boolean userEnabled = ((Switch)findViewById(R.id.switchUserEnabled)).isChecked();
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            ServerConnection.AddDevice(deviceId, name, sensorId, sensorType, lowestStats, highestStats, userEnabled, selectedType);
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_control_device_config);

        Intent intent = getIntent();
        username = intent.getStringExtra("EXTRA_USERNAME");
        LinearLayout layout = findViewById(R.id.linearLayoutDeviceInfo);
        while (layout.getChildCount() > 6) {
            layout.removeViewAt(6);
        }
        Button button = findViewById(R.id.button);
        button.setText("Add");
        spinner = findViewById(R.id.spinner);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                JSONArray response = ServerConnection.GetSensors(username, this);
                for (int i = 0; i < response.length(); i++) {
                    Sensor s = new Sensor(response.getJSONObject(i).getString("sensor_type"), response.getJSONObject(i).getString("id"));
                    sensorList.add(s);
                }
                ex.shutdown();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        });
        try {
            ex.awaitTermination(10, TimeUnit.SECONDS);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        sensorListAdapter = new ArrayAdapter<Sensor>(this, R.layout.support_simple_spinner_dropdown_item, sensorList);
        spinner.setAdapter(sensorListAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Sensor s = (Sensor)parent.getItemAtPosition(position);
                TextView textViewSensorType = findViewById(R.id.textViewSensorType);
                TextView textViewSensorID = findViewById(R.id.textViewSensorID);
                textViewSensorType.setText(s.type);
                textViewSensorID.setText(s.id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2 = findViewById(R.id.spinner2);
        deviceTypesAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, deviceTypes);
        spinner2.setAdapter(deviceTypesAdapter);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedType = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}