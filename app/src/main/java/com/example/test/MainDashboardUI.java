package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainDashboardUI extends AppCompatActivity {
    SharedPreferences mPrefs;
    String username;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dashboard_ui);

        mPrefs = getSharedPreferences("dadn_storage", 0);
        ctx = getApplicationContext();
        username = mPrefs.getString("user","UserNotFound");

        TextView userFullNameText = (TextView) findViewById(R.id.UserFullNameText);
        userFullNameText.setText(ServerConnection.GetUserFullname(username));
    }

    public void GoToSensorsUI(View v)
    {
        Intent intent = new Intent(this, MainSensorsUI.class);
        startActivity(intent);
    }
}