package com.example.test;

import java.util.Calendar;

import static java.lang.System.lineSeparator;

public class AdafruitResponse {
    public String id;

    public String value;

    public String feed_key;

    public String created_at;

    public String GetCreationDate()
    {
        String str = this.created_at.replaceAll("[^0-9]","S");
        String[] arr = str.split("S");
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(arr[0]) ,Integer.parseInt(arr[1]),Integer.parseInt(arr[2]),Integer.parseInt(arr[3]),Integer.parseInt(arr[4]));
        cal.add(Calendar.HOUR_OF_DAY,7);
        String res = "";
        res += cal.get(Calendar.DATE) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR);
        res += "-";
        res += cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        return res;

    }
}
