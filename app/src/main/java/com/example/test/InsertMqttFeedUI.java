package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.net.MalformedURLException;

public class InsertMqttFeedUI extends AppCompatActivity {
    SharedPreferences mPrefs;
    String username;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_mqtt_feed);
        mPrefs = getSharedPreferences("dadn_storage", 0);
        ctx = getApplicationContext();
        username = mPrefs.getString("user","UserNotFound");

        TextView userFullNameText = (TextView) findViewById(R.id.UserFullNameText);
        userFullNameText.setText(ServerConnection.GetUserFullname(username));

    }

    public void Insert(View v) throws MalformedURLException {
        ServerConnection.InsertMqttFeed(
                username,
                ((EditText)findViewById(R.id.FeedUsernameEditText)).getText().toString(),
                ((EditText)findViewById(R.id.FeedKeyEditText)).getText().toString(),
                ((EditText)findViewById(R.id.FeedNameEditText)).getText().toString()
        );
        GoBack();
    }

    public void GoBack(View v)
    {
        GoBack();
    }

    public void GoBack()
    {
        Intent intent = new Intent(this, LoginUI.class);
        startActivity(intent);
    }
}