package com.example.test;

import android.content.Context;
import android.graphics.Color;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;

public class ServerConnection {
    //Change the host for future testing
    static String host = "http://192.168.1.47:8080"; //the host at port 8080
    static String host5000 = " https://2b02666d9879ff.localhost.run"; //the host at port 5000
//    static String host = "localhost";

    public static boolean AuthenticateUser(String username, String password) throws InterruptedException {
        boolean res = false;
        String GetUrl = host + "/auth";
        GetUrl += "?username=" + username;
        GetUrl += "&password=" + password;
        URL obj = null;
        try {
            obj = new URL(GetUrl);
            System.out.println("Sending Get: " + GetUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setConnectTimeout(5000); //ms
            System.out.println(con.getRequestMethod());
            int responseCode = con.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                String resString = response.toString();
                System.out.println("Result String: " + resString);
                if (resString.equals("valid"))
                    res = true;

            } else {
                System.out.println("GET request not worked");
            }
        } catch (SocketTimeoutException e) {

        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static MQTTService GetMqttInfo(String username, String password, Context context) throws InterruptedException {
        MQTTService mqttService = new MQTTService(context);
        String result = "";
        try {
            String query = String.format("username=%s&password=%s", URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"));


            String url = host + "/get_mqtt_info";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
            System.out.println(result);
            JSONArray response = new JSONArray(result);
            mqttService.username(response.getJSONObject(0).getString("username"));
            mqttService.password(response.getJSONObject(0).getString("feed_key"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mqttService;
    }

    public static JSONArray GetDeviceStates(String username, String password, String mqttName, Context context) throws InterruptedException {
        String result = "";
        JSONArray response = new JSONArray();
        try {
            String query = String.format("username=%s&password=%s&mqtt_name=%s", URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"),  URLEncoder.encode(mqttName, "UTF-8"));

            String url = host + "/get_device_states";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
            System.out.println(result);
            response = new JSONArray(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static JSONArray GetControlDeviceInfo(String username, String password, String controlDeviceId, Context context) throws InterruptedException {
        String result = "";
        JSONArray response = new JSONArray();
        try {
            String query = String.format("username=%s&password=%s&control_device_id=%s", URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"), URLEncoder.encode(controlDeviceId, "UTF-8"));

            String url = host + "/get_control_device_info";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
            System.out.println(result);
            response = new JSONArray(result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static JSONArray GetSensors(String username, Context context) throws InterruptedException {
        String result = "";
        JSONArray response = new JSONArray();
        try {
            String query = String.format("username=%s", URLEncoder.encode(username, "UTF-8"));

            String url = host + "/get_sensors";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
            System.out.println(result);
            response = new JSONArray(result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static JSONArray GetSensorStats(String sensorId, String sensorType) throws InterruptedException {
        String result = "";
        JSONArray response = new JSONArray();
        try {
            String query = String.format("sensor_id=%s&sensor_type=%s", URLEncoder.encode(sensorId, "UTF-8"), URLEncoder.encode(sensorType, "UTF-8"));

            String url = host + "/get_sensor_stats";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
            System.out.println(result);
            response = new JSONArray(result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static void AddDevice(String deviceId, String name, String sensorId, String sensorType, String lowestStats, String highestStats, boolean userEnabled, String selectedType) {
        String result = "";
        String enabled = "0";
        if (userEnabled) {
            enabled = "1";
        }
        try {

            String query = String.format(
                    "control_device_id=%s&name=%s&sensor_id=%s&sensor_type=%s&lowest_stats=%s&highest_stats=%s&user_enabled=%s&type=%s",
                    URLEncoder.encode(deviceId, "UTF-8"),
                    URLEncoder.encode(name, "UTF-8"),
                    URLEncoder.encode(sensorId, "UTF-8"),
                    URLEncoder.encode(sensorType, "UTF-8"),
                    URLEncoder.encode(lowestStats, "UTF-8"),
                    URLEncoder.encode(highestStats, "UTF-8"),
                    URLEncoder.encode(enabled, "UTF-8"),
                    URLEncoder.encode(selectedType, "UTF-8")
            );

            String url = host + "/add_control_device";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void DeleteDevice(int id) {
        String result = "";
        try {
            String query = String.format("control_device_id=%s", URLEncoder.encode(String.valueOf(id), "UTF-8"));
            String url = host + "/delete_device";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void UpdateControlDevice(String controlDeviceId, String name, String sensorId, String sensorType, String lowestStats, String highestStats, boolean userEnabled, String timerList, String deviceId, String selectedType) {
        String result = "";
        String enabled = "0";
        if (userEnabled) {
            enabled = "1";
        }
        try {

            String query = String.format(
                    "control_device_id=%s&name=%s&sensor_id=%s&sensor_type=%s&lowest_stats=%s&highest_stats=%s&user_enabled=%s&timer_list=%s&device_id=%s&type=%s",
                    URLEncoder.encode(controlDeviceId, "UTF-8"),
                    URLEncoder.encode(name, "UTF-8"),
                    URLEncoder.encode(sensorId, "UTF-8"),
                    URLEncoder.encode(sensorType, "UTF-8"),
                    URLEncoder.encode(lowestStats, "UTF-8"),
                    URLEncoder.encode(highestStats, "UTF-8"),
                    URLEncoder.encode(enabled, "UTF-8"),
                    URLEncoder.encode(timerList, "UTF-8"),
                    URLEncoder.encode(deviceId, "UTF-8"),
                    URLEncoder.encode(selectedType, "UTF-8")
            );

            String url = host + "/update_control_device";
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            InputStream in = connection.getInputStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            result = s.hasNext() ? s.next() : "";
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
        public static String GetUserFullname(String username)
    {
        final String[] res = new String[1];
        String GetUrl = host5000 + "/get_user_full_name";
        GetUrl += "?username=" + username;
        final String finGetUrl = GetUrl;
        Thread GetNameThread = new Thread(()->{
            try {
                final URL obj;
                obj = new URL(finGetUrl);
                System.out.println("Sending Get: " + finGetUrl);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                System.out.println(con.getRequestMethod());
                int responseCode = con.getResponseCode();
                System.out.println("GET Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String resString = response.toString();
                    res[0] = (new JSONObject(resString)).getString("result");


                } else {
                    res[0] = "User Not Found";
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        GetNameThread.start();
        try{
            GetNameThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        return res[0];
    }

    public static JSONArray GetSensorsInfoBriefs(String username)
    {
        final JSONArray[] res = new JSONArray[1];
        String GetUrl = host5000 + "/get_user_sensors_brief";
        GetUrl += "?username=" + username;
        final String finGetUrl = GetUrl;
        Thread GetSensorsThread = new Thread(()->{
            try {
                final URL obj;
                obj = new URL(finGetUrl);
                System.out.println("Sending Get: " + finGetUrl);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                System.out.println(con.getRequestMethod());
                int responseCode = con.getResponseCode();
                System.out.println("GET Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String resString = response.toString();
                    res[0] = new JSONArray(resString);


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        GetSensorsThread.start();
        try{
            GetSensorsThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        return res[0];
    }

    public static JSONArray GetActuatorsInfoBriefs(String username)
    {
        final JSONArray[] res = new JSONArray[1];
        String GetUrl = host5000 + "/get_user_actuators_brief";
        GetUrl += "?username=" + username;
        final String finGetUrl = GetUrl;
        Thread GetActuatorsThread = new Thread(()->{
            try {
                final URL obj;
                obj = new URL(finGetUrl);
                System.out.println("Sending Get: " + finGetUrl);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                System.out.println(con.getRequestMethod());
                int responseCode = con.getResponseCode();
                System.out.println("GET Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String resString = response.toString();
                    res[0] = new JSONArray(resString);


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        GetActuatorsThread.start();
        try{
            GetActuatorsThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        return res[0];
    }

    public static JSONObject GetUserDevicesCount(String username)
    {
        final JSONObject[] res = new JSONObject[1];
        String GetUrl = host5000 + "/get_user_devices_count";
        GetUrl += "?username=" + username;
        final String finGetUrl = GetUrl;
        Thread GetSensorCountThread = new Thread(()->{
            try {
                final URL obj;
                obj = new URL(finGetUrl);
                System.out.println("Sending Get: " + finGetUrl);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                System.out.println(con.getRequestMethod());
                int responseCode = con.getResponseCode();
                System.out.println("GET Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String resString = response.toString();
                    res[0] = new JSONObject(resString);


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        GetSensorCountThread.start();
        try{
            GetSensorCountThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        return res[0];
    }

    public static void InsertMqttFeed(String owner,String feedUsername, String feedPassword, String feedName) throws MalformedURLException {
        final JSONObject[] res = new JSONObject[1];
        String Url = host5000 + "/insert_mqtt_feed";
        final URL obj;
        obj = new URL(Url);
        Thread InsertThread = new Thread(()->{
            try {

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json; utf-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);


                JSONObject jsonObj = new JSONObject();
                jsonObj.put("username",owner);
                jsonObj.put("feed_username",feedUsername);
                jsonObj.put("password",feedPassword);
                jsonObj.put("feed_name",feedName);

                try(OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonObj.toString().getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                con.getResponseCode();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        InsertThread.start();
        try{
            InsertThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }
        System.out.println("asdasdasd");

    }

        public static Integer InsertSensor(String owner,String feedUsername, String feedName, String sensorType) throws MalformedURLException {
        if (owner.equals("") || feedUsername.equals("") || feedName.equals("") || sensorType.equals(""))
        {
            return 500;
        }
        final Integer[] res = new Integer[1];
        String Url = host5000 + "/insert_sensor";
        final URL obj;
        obj = new URL(Url);
        Thread InsertThread = new Thread(()->{
            try {

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json; utf-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);


                JSONObject jsonObj = new JSONObject();
                jsonObj.put("username",owner);
                jsonObj.put("feed_username",feedUsername);
                jsonObj.put("sensor_type",sensorType);
                jsonObj.put("feed_name",feedName);

                try(OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonObj.toString().getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                res[0] = con.getResponseCode();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        InsertThread.start();
        try{
            InsertThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }
        return res[0];

    }

    public static JSONArray GetUserMqttFeedUsernames(String username)
    {
        final JSONArray[] res = new JSONArray[1];
        String GetUrl = host5000 + "/get_user_mqtt_feed_username";
        GetUrl += "?username=" + username;
        final String finGetUrl = GetUrl;
        Thread getUsernamesThread = new Thread(()->{
            try {
                final URL obj;
                obj = new URL(finGetUrl);
                System.out.println("Sending Get: " + finGetUrl);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                System.out.println(con.getRequestMethod());
                int responseCode = con.getResponseCode();
                System.out.println("GET Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String resString = response.toString();
                    res[0] = new JSONArray(resString);


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        getUsernamesThread.start();
        try{
            getUsernamesThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        return res[0];
    }

    public static JSONArray GetFeedNamesList(String owner, String feedUsername)
    {
        final JSONArray[] res = new JSONArray[1];
        String GetUrl = host5000 + "/get_feed_name_from_username";
        GetUrl += "?username=" + owner;
        GetUrl += "&feed_username=" + feedUsername;
        final String finGetUrl = GetUrl;
        Thread getFeednamesThread = new Thread(()->{
            try {
                final URL obj;
                obj = new URL(finGetUrl);
                System.out.println("Sending Get: " + finGetUrl);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                System.out.println(con.getRequestMethod());
                int responseCode = con.getResponseCode();
                System.out.println("GET Response Code :: " + responseCode);
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String resString = response.toString();
                    res[0] = new JSONArray(resString);


                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        getFeednamesThread.start();
        try{
            getFeednamesThread.join();
        }
        catch (InterruptedException e) {
            System.out.println(e);
        }

        return res[0];
    }
}

