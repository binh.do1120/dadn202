package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import android.util.Log;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.test.MESSAGE";
    public static final String EXTRA_USERNAME = "com.example.test.USERNAME";
    public static final String EXTRA_PASSWORD = "com.example.test.PASSWORD";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
    }

    public void loginButtonOnclick(View view) {
        //get login info
        EditText editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        String username = editTextUsername.getText().toString();
        String password = editTextPassword.getText().toString();

        //loop on thread
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            String result = "";
            Intent intent = new Intent(this, MainPageActivity.class);
            try {
                String query = String.format("username=%s&password=%s", URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"));

                //home pc address + port
                String url = "http://192.168.1.47:8080/auth";
                URLConnection connection = new URL(url + "?" + query).openConnection();
                connection.setRequestProperty("Accept-Charset", "UTF-8");
                InputStream in = connection.getInputStream();
                Scanner s = new Scanner(in).useDelimiter("\\A");
                result = s.hasNext() ? s.next() : "";
                System.out.println(result);
                if (result.equals("valid")) {
                    intent.putExtra(EXTRA_USERNAME, username);
                    intent.putExtra(EXTRA_PASSWORD, password);
                    startActivity(intent);
                }
                else {
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Wrong username or password", Toast.LENGTH_LONG).show()
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        /* HTTP stuff
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            String result = "";
            try {
                String query = String.format("username=%s&password=%s", URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"));

                //home pc address + port
                URL url = new URL("http://192.168.1.47:8080/login");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setReadTimeout(10000); // ms
                connection.setConnectTimeout(15000); // ms
                connection.setRequestProperty("Accept-Charset", "UTF-8");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + "UTF-8");
                OutputStream output = connection.getOutputStream();
                output.write(query.getBytes("UTF-8"));
                InputStream in = new BufferedInputStream(connection.getInputStream());
                Scanner s = new Scanner(in).useDelimiter("\\A");
                result = s.hasNext() ? s.next() : "";
            } catch (Exception e) {
                e.printStacktrace();
            }
        });
        */
    }
}
