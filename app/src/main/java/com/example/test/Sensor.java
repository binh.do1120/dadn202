package com.example.test;

public class Sensor {
    public String type;
    public String id;

    Sensor(String _type, String _id) {
        type = _type;
        id = _id;
    }

    @Override
    public String toString() {
        return type + " " + id;
    }
}
