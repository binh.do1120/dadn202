package com.example.test;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ControlDeviceConfig extends AppCompatActivity {
    private String username ="";
    private String password = "";
    private String controlDeviceId = "";
    private Spinner spinner;
    private Spinner spinner2;
    private ArrayList<Sensor> sensorList = new ArrayList<Sensor>();
    private ArrayAdapter<Sensor> sensorListAdapter;
    private String[] deviceTypes = {"light", "water"};
    private String selectedType = "";
    private ArrayAdapter<String> deviceTypesAdapter;
    private int numTimers = 0;

    public void submitListener(View v) {
        String name = ((EditText)findViewById(R.id.textViewDeviceName)).getText().toString();
        String deviceId = ((EditText)findViewById(R.id.textViewDeviceId)).getText().toString();
        String sensorId = ((TextView)findViewById(R.id.textViewSensorID)).getText().toString();
        String sensorType = ((TextView)findViewById(R.id.textViewSensorType)).getText().toString();
        String lowestStats = ((EditText)findViewById(R.id.textViewSensorMin)).getText().toString();
        String highestStats = ((EditText)findViewById(R.id.textViewSensorMax)).getText().toString();
        boolean userEnabled = ((Switch)findViewById(R.id.switchUserEnabled)).isChecked();
        String timerList = "[";
        LinearLayout timers = findViewById(R.id.linearLayoutTimeList);
        for (int i = 0; i < timers.getChildCount(); i++) {
            ConstraintLayout row = (ConstraintLayout)timers.getChildAt(i);
            TextView rowText = (TextView)row.findViewById(i + 1);
            String[] time = rowText.getText().toString().replaceAll(" ","").split("-");
            if (i != 0) {
                timerList += ",";
            }
            timerList += "{\"start_time\":\"" + time[0] + "\",\"end_time\":\"" + time[1] + "\"}";
        }
        timerList += "]";
        ExecutorService ex = Executors.newSingleThreadExecutor();
        String finalTimerList = timerList;
        ex.submit(() -> {
            ServerConnection.UpdateControlDevice(controlDeviceId, name, sensorId, sensorType, lowestStats, highestStats, userEnabled, finalTimerList, deviceId, selectedType);
        });
    }

    public void addListener(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setTitle("Set time");
        View viewInflated = LayoutInflater.from(getBaseContext()).inflate(R.layout.text_input_time, (ViewGroup)findViewById(android.R.id.content), false);
        final EditText editTextTimeStartHour = (EditText) viewInflated.findViewById(R.id.editTextTimeStartHour);
        final EditText editTextTimeStartMinute = (EditText) viewInflated.findViewById(R.id.editTextTimeStartMinute);
        final EditText editTextTimeEndHour = (EditText) viewInflated.findViewById(R.id.editTextTimeEndHour);
        final EditText editTextTimeEndMinute = (EditText) viewInflated.findViewById(R.id.editTextTimeEndMinute);
        builder.setView(viewInflated);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                LinearLayout timers = findViewById(R.id.linearLayoutTimeList);
                ConstraintLayout row = new ConstraintLayout(v.getContext());
                row.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                );
                row.setId(numTimers + 1000);
                ImageButton buttonRow = new ImageButton(v.getContext());
                row.addView(buttonRow);

                buttonRow.setLayoutParams(new ConstraintLayout.LayoutParams(
                        100,
                        100)
                );
                buttonRow.setBackground(getResources().getDrawable(android.R.drawable.ic_menu_delete));
                buttonRow.setOnClickListener(deleteClickListener);
                TextView textViewRow = new TextView(v.getContext());
                row.addView(textViewRow);
                textViewRow.setLayoutParams(new ConstraintLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                );
                textViewRow.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                textViewRow.setOnClickListener(timerClickListener);
                ((ConstraintLayout.LayoutParams)buttonRow.getLayoutParams()).rightToRight = numTimers + 1000;
                ((ConstraintLayout.LayoutParams)textViewRow.getLayoutParams()).leftToLeft = numTimers + 1000;
                buttonRow.setId(numTimers);
                textViewRow.setId(numTimers + 1);

                numTimers++;
                timers.addView(row);

                textViewRow.setText(
                        editTextTimeStartHour.getText() + ":" + editTextTimeStartMinute.getText() + ":00"
                                + "   -   " + editTextTimeEndHour.getText() + ":" + editTextTimeEndMinute.getText() + ":00"
                );
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    View.OnClickListener timerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            TextView rowText = (TextView)v;
            builder.setTitle("Set time");
            View viewInflated = LayoutInflater.from(getBaseContext()).inflate(R.layout.text_input_time, (ViewGroup)findViewById(android.R.id.content), false);
            final EditText editTextTimeStartHour = (EditText) viewInflated.findViewById(R.id.editTextTimeStartHour);
            final EditText editTextTimeStartMinute = (EditText) viewInflated.findViewById(R.id.editTextTimeStartMinute);
            final EditText editTextTimeEndHour = (EditText) viewInflated.findViewById(R.id.editTextTimeEndHour);
            final EditText editTextTimeEndMinute = (EditText) viewInflated.findViewById(R.id.editTextTimeEndMinute);
            builder.setView(viewInflated);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    rowText.setText(
                            editTextTimeStartHour.getText() + ":" + editTextTimeStartMinute.getText() + ":00"
                            + "   -   " + editTextTimeEndHour.getText() + ":" + editTextTimeEndMinute.getText() + ":00"
                    );
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
    };

    View.OnClickListener deleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout timers = findViewById(R.id.linearLayoutTimeList);
            int target = (int)v.getId() + 1000;
            for (int i = 0; i < timers.getChildCount(); i++) {
                if (timers.getChildAt(i).getId() == target) {
                    timers.removeViewAt(i);
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_control_device_config);

        Intent intent = getIntent();
        username = intent.getStringExtra("EXTRA_USERNAME");
        password = intent.getStringExtra("EXTRA_PASSWORD");
        controlDeviceId = intent.getStringExtra("EXTRA_DEVICE_ID");
        EditText deviceId = findViewById(R.id.textViewDeviceId);
        deviceId.setText(controlDeviceId);
        spinner = findViewById(R.id.spinner);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                JSONArray response = ServerConnection.GetSensors(username, this);
                for (int i = 0; i < response.length(); i++) {
                    Sensor s = new Sensor(response.getJSONObject(i).getString("sensor_type"), response.getJSONObject(i).getString("id"));
                    sensorList.add(s);
                }
                ex.shutdown();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        });
        try {
            ex.awaitTermination(10, TimeUnit.SECONDS);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        sensorListAdapter = new ArrayAdapter<Sensor>(this, R.layout.support_simple_spinner_dropdown_item, sensorList);
        spinner.setAdapter(sensorListAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Sensor s = (Sensor)parent.getItemAtPosition(position);
                TextView textViewSensorType = findViewById(R.id.textViewSensorType);
                TextView textViewSensorID = findViewById(R.id.textViewSensorID);
                textViewSensorType.setText(s.type);
                textViewSensorID.setText(s.id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2 = findViewById(R.id.spinner2);
        deviceTypesAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, deviceTypes);
        spinner2.setAdapter(deviceTypesAdapter);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedType = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        EditText textViewDeviceName = findViewById(R.id.textViewDeviceName);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                JSONArray response = ServerConnection.GetControlDeviceInfo(username, password, controlDeviceId, this);
                runOnUiThread(() -> {
                    try {
                        TextView textViewSensorType = findViewById(R.id.textViewSensorType);
                        TextView textViewSensorID = findViewById(R.id.textViewSensorID);
                        EditText textViewSensorMin = findViewById(R.id.textViewSensorMin);
                        EditText textViewSensorMax = findViewById(R.id.textViewSensorMax);
                        Switch switchUserEnabled = findViewById(R.id.switchUserEnabled);

                        switchUserEnabled.setChecked(response.getJSONObject(0).getInt("user_enabled") == 1);
                        String sensorId = String.valueOf(response.getJSONObject(0).getInt("sensor_id"));
                        String sensorType = response.getJSONObject(0).getString("sensor_type");
                        textViewSensorID.setText(sensorId);
                        textViewSensorType.setText(sensorType);
                        textViewSensorMin.setText(String.valueOf(response.getJSONObject(0).getDouble("lowest_stats")));
                        textViewSensorMax.setText(String.valueOf(response.getJSONObject(0).getDouble("highest_stats")));
                        int sensorIndex = 0;
                        for (sensorIndex = 0; sensorIndex < sensorList.size(); sensorIndex++) {
                            if (sensorList.get(sensorIndex).type.equals(sensorType) && sensorList.get(sensorIndex).id.equals(sensorId)) {
                                break;
                            }
                        }
                        spinner.setSelection(sensorIndex);
                        JSONArray timeList = response.getJSONObject(0).getJSONArray("time");
                        LinearLayout timers = findViewById(R.id.linearLayoutTimeList);
                        timers.removeAllViewsInLayout();
                        for (int i = 0; i < timeList.length(); i++) {
                            int hourStart = timeList.getJSONObject(i).getInt("start_time") / 3600;
                            int minuteStart = (timeList.getJSONObject(i).getInt("start_time") / 60) % 60;
                            int secondStart = timeList.getJSONObject(i).getInt("start_time") % 60;
                            int hourEnd = timeList.getJSONObject(i).getInt("end_time") / 3600;
                            int minuteEnd = (timeList.getJSONObject(i).getInt("end_time") / 60) % 60;
                            int secondEnd = timeList.getJSONObject(i).getInt("end_time") % 60;

                            ConstraintLayout row = new ConstraintLayout(this);
                            row.setLayoutParams(new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT)
                            );
                            row.setId(i + 1000);
                            ImageButton buttonRow = new ImageButton(this);
                            row.addView(buttonRow);

                            buttonRow.setLayoutParams(new ConstraintLayout.LayoutParams(
                                    100,
                                    100)
                            );
                            buttonRow.setBackground(getResources().getDrawable(android.R.drawable.ic_menu_delete));
                            buttonRow.setOnClickListener(deleteClickListener);
                            TextView textViewRow = new TextView(this);
                            row.addView(textViewRow);
                            textViewRow.setLayoutParams(new ConstraintLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT)
                            );
                            String rowText = String.valueOf(hourStart);
                            rowText += ":" + String.valueOf(minuteStart);
                            rowText += ":" + String.valueOf(secondStart);
                            rowText += "   -   " + String.valueOf(hourEnd);
                            rowText += ":" + String.valueOf(minuteEnd);
                            rowText += ":" + String.valueOf(secondEnd);
                            textViewRow.setText(rowText);
                            textViewRow.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                            textViewRow.setOnClickListener(timerClickListener);
                            ((ConstraintLayout.LayoutParams)buttonRow.getLayoutParams()).rightToRight = i + 1000;
                            ((ConstraintLayout.LayoutParams)textViewRow.getLayoutParams()).leftToLeft = i + 1000;
                            buttonRow.setId(i);
                            textViewRow.setId(i + 1);

                            timers.addView(row);
                        }
                        numTimers = timers.getChildCount();
                        textViewDeviceName.setText(response.getJSONObject(0).getString("name"));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}