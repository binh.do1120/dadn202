package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.graphics.Color;
import android.widget.Button;
import android.widget.TextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import android.os.Bundle;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class DataHistoryUI extends AppCompatActivity implements
        View.OnClickListener {
    static String broker = "tcp://io.adafruit.com:1883";
//    static String username = "dolethienan2000";
//    static String password = "aio_fcCw17wPdFwY1p4LLSxW7vb4Xlvi";

    TextView startDatePicker, startTimePicker,endDatePicker,endTimePicker;
    EditText txtDate, txtTime;
    Calendar startCal = Calendar.getInstance();
    Calendar endCal = Calendar.getInstance();
    int curYear = startCal.get(Calendar.YEAR);
    int curMonth = startCal.get(Calendar.MONTH) + 1;
    int curDate = startCal.get(Calendar.DATE);
    int curHour = startCal.get(Calendar.HOUR_OF_DAY);
    int curMinute = startCal.get(Calendar.MINUTE);
    static List<String> xAxisData;
    static List<Integer> yAxisData;

    LineChartView chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_history_screeen);

        chart = findViewById(R.id.chart);

        xAxisData = new ArrayList<String>();
        yAxisData = new ArrayList<Integer>();
        //Set the date and time
        startDatePicker=(TextView)findViewById(R.id.startDatePickerText);
        startTimePicker=(TextView)findViewById(R.id.startTimePickerText);
        endDatePicker=(TextView)findViewById(R.id.endDatePickerText);
        endTimePicker=(TextView)findViewById(R.id.endTimePickerText);

        startDatePicker.setText(GetDateFromCal(startCal));
        endDatePicker.setText(GetDateFromCal(endCal));
        startTimePicker.setText(GetTimeFromCal(startCal));
        endTimePicker.setText(GetTimeFromCal(endCal));

//        List yAxisValues = new ArrayList();
//        List xAxisValues = new ArrayList();
//        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));
//
//        String[] xAxisData = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept",
//                "Oct", "Nov", "Dec"};
//        Integer[] yAxisData = {50, 20, 15, 30, 20, 60, 15, 40, 45, 10, 90, 18};
//        for(int i = 0; i < xAxisData.length; i++){
//            xAxisValues.add(i, new AxisValue(i).setLabel(xAxisData[i]));
//        }
//
//        for (int i = 0; i < yAxisData.length; i++){
//            yAxisValues.add(new PointValue(i, yAxisData[i]));
//        }
//        List lines = new ArrayList();
//        lines.add(line);
//        LineChartData data = new LineChartData();
//        data.setLines(lines);
//        Axis xAxis = new Axis();
//        xAxis.setValues(xAxisValues);
//        data.setAxisXBottom(xAxis);
//        Axis yAxis = new Axis();
//        data.setAxisYLeft(yAxis);
//        xAxis.setTextSize(10);
//        xAxis.setTextColor(Color.parseColor("#03A9F4"));
//        yAxis.setTextColor(Color.parseColor("#03A9F4"));
//        yAxis.setTextSize(10);
//        yAxis.setName("Units");
//        xAxis.setName("Time");
//        chart.setLineChartData(data);


    }

    public void UpdateChartFunc(View v) throws InterruptedException {
        //Draw the chart

//        xAxisData = new ArrayList<String>();
//        yAxisData = new ArrayList<Integer>();
        Calendar GMTstartCal = (Calendar) startCal.clone();
        GMTstartCal.add(Calendar.HOUR_OF_DAY,-7);
        Calendar GMTendCal = (Calendar) endCal.clone();
        GMTendCal.add(Calendar.HOUR_OF_DAY,-7);

//        new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                String GET_URL = "http://dadn.esp32thanhdanh.link/?fbclid=IwAR1udH9cIsE9bMv2FP93bJUiVyjP0KdBc8VGLxFn8IiwbG8yMdcDqB1zt54";
//
//
//                try {
//                    String username="CSE_BBC";
//                    String password="";
//                    String username1="CSE_BBC1";
//                    String password1="";
//                    String feedKey="";
//                    URL obj = new URL(GET_URL);
//                    HttpURLConnection con = null;
//                    con = (HttpURLConnection) obj.openConnection();
//                    System.out.println(con.getContent());
//                    int responseCode = con.getResponseCode();
//                    if (responseCode == HttpURLConnection .HTTP_OK) { // success
//                        BufferedReader in = new BufferedReader(new InputStreamReader(
//                                con.getInputStream()));
//                        String inputLine;
//                        StringBuffer response = new StringBuffer();
//
//                        while ((inputLine = in.readLine()) != null) {
//                            response.append(inputLine);
//                            // System.out.println(inputLine);
//                        }
//                        in.close();
//                        String resString = response.toString();
//                        ObjectMapper mapper = new ObjectMapper();
//                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//                        KeysResponse resKeys = mapper.readValue(resString, KeysResponse.class);
//                        password = resKeys.keyBBC;
//                        password1 = resKeys.keyBBC1;
//
//
//
//                    } else {
//                        System.out.println("GET request for username and password not worked");
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });
        String username="CSE_BBC";
        String password="aio_IxzX60sNBm6UQtbS54mKpj4EdCYM";
        String username1="CSE_BBC1";
        String password1="aio_dfWg39AvGDolD7rPm73T6GNha9Bc";
        String feedKey="";
        GetFeedData(username1,password1,"bk-iot-sound",new Time(GMTstartCal),new Time(GMTendCal));

    }

    @Override
    public void onClick(View v) {

        if (v == startDatePicker) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            startCal.set(year,monthOfYear,dayOfMonth);
                            if (startCal.compareTo(endCal) == 1)
                            {
                                startCal = (Calendar) endCal.clone();
                                startTimePicker.setText(GetTimeFromCal(startCal));
                            }
                            startDatePicker.setText(GetDateFromCal(startCal));

                        }
                    }, curYear, curMonth, curDate);
            datePickerDialog.show();
        }

        if (v == endDatePicker) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            endCal.set(year,monthOfYear,dayOfMonth);
                            if (endCal.compareTo(startCal) == -1)
                            {
                                endCal = (Calendar) startCal.clone();
                                endTimePicker.setText(GetTimeFromCal(endCal));
                            }
                            endDatePicker.setText(GetDateFromCal(endCal));

                        }
                    }, curYear, curMonth, curDate);
            datePickerDialog.show();
        }
        if (v == startTimePicker) {
            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            startCal.set(Calendar.HOUR_OF_DAY,hourOfDay);
                            startCal.set(Calendar.MINUTE,minute);
                            if(startCal.compareTo(endCal) == 1)
                            {
                                startCal = (Calendar) endCal.clone();
                                startDatePicker.setText(GetDateFromCal(startCal));
                            }
                            startTimePicker.setText(GetTimeFromCal(startCal));
                        }
                    }, curHour, curMinute, false);
            timePickerDialog.show();
        }

        if (v == endTimePicker) {
            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            endCal.set(Calendar.HOUR_OF_DAY,hourOfDay);
                            endCal.set(Calendar.MINUTE,minute);
                            if(endCal.compareTo(startCal) == -1)
                            {
                                endCal = (Calendar) startCal.clone();
                                endDatePicker.setText(GetDateFromCal(endCal));
                            }
                            endTimePicker.setText(GetTimeFromCal(endCal));
                        }
                    }, curHour, curMinute, false);
            timePickerDialog.show();
        }
    }

    static String GetDateFromCal(Calendar cal)
    {
        return cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
    }

    static String GetTimeFromCal(Calendar cal)
    {
        return cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
    }

    static MqttMessage Message(String id,String name, String data)
    {
        String ret = "";
        ret += "{\"id\":\"";
        ret += id;
        ret += "\",\"name\":\"";
        ret += name;
        ret += "\",\"data\":\"";
        ret += data;
        ret += "\",\"unit\":\"\"}";

        return new MqttMessage(ret.getBytes());

    }

    static MqttMessage Message(Integer id, String name, String data)
    {
        return Message(id.toString(), name, data);
    }

    static String GetURLQuery(String username,String password, String feedKey, Time startTime, Time endTime)
    {
        String res = "https://io.adafruit.com/api/v2/" + username + "/feeds/" + feedKey + "/data";
        res += "?x-aio-key=" + password;
        if (!startTime.IsNull())
        {
            res += "&start_time="+startTime.toString();
        }
        if (!endTime.IsNull())
        {
            res += "&end_time="+endTime.toString();
        }
        return res;
    }


    void GetFeedData(String username,String password,String feedKey, Time startTime, Time endTime) {
        // String GET_URL = "https://io.adafruit.com/api/v2/dolethienan2000/feeds/dadn/data?x-aio-key=aio_fcCw17wPdFwY1p4LLSxW7vb4Xlvi&start_time=2021-5-27T00:00Z&end_time=2021-05-27T09:00Z";
        xAxisData = new ArrayList<String>();
        yAxisData = new ArrayList<Integer>();
        new Thread(new Runnable() {
            public void run() {
                URL obj = null;
                try {

                    String GET_URL = GetURLQuery(username,password,feedKey, startTime, endTime);
                    // System.out.println(GET_URL);
                    // GET_URL = GetURLQuery(feedKey, startTime, endTime);
                    System.out.println("the GET URL: " + GET_URL);
                    obj = new URL(GET_URL);
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    System.out.println(con.getRequestMethod());
                    int responseCode = con.getResponseCode();
                    System.out.println("GET Response Code :: " + responseCode);
                    if (responseCode == HttpURLConnection .HTTP_OK) { // success
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                            // System.out.println(inputLine);
                        }
                        in.close();
                        String resString = response.toString();
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        AdafruitResponse[] resArray = mapper.readValue(resString, AdafruitResponse[].class);
                        List yAxisValues = new ArrayList();
                        List xAxisValues = new ArrayList();
                        Collections.reverse(Arrays.asList(resArray));
                        if(resArray.length >= 1)
                        {
                            for (AdafruitResponse adafruitResponse : resArray) {
                                xAxisData.add(adafruitResponse.GetCreationDate());
                                DataPoint arr = mapper.readValue((adafruitResponse.value).toString(), DataPoint.class);
                                yAxisData.add(arr.data);
                            }
                            for(int i = 0; i <= xAxisData.toArray().length-1 ; i++){
                                xAxisValues.add(i, new AxisValue(i).setLabel(xAxisData.get(i).toString()));
                            }

                            for (int i = 0; i <= yAxisData.toArray().length-1 ; i++){
                                yAxisValues.add(new PointValue(i, yAxisData.get(i)));
                            }
                        }
                        else
                        {
                            Integer i = 0;
                            xAxisValues.add(i, new AxisValue(i).setLabel("NULL"));
                            yAxisValues.add(new PointValue(i,0));
                        }
                        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));
                        List lines = new ArrayList();
                        lines.add(line);
                        LineChartData data = new LineChartData();
                        data.setLines(lines);
                        Axis xAxis = new Axis();
                        xAxis.setValues(xAxisValues);
                        data.setAxisXBottom(xAxis);
                        Axis yAxis = new Axis();
                        data.setAxisYLeft(yAxis);
                        xAxis.setTextSize(10);
                        xAxis.setTextColor(Color.parseColor("#03A9F4"));
                        yAxis.setTextColor(Color.parseColor("#03A9F4"));
                        yAxis.setTextSize(10);
                        yAxis.setName("Units");
                        xAxis.setName("Time");
                        chart.setLineChartData(data);

                    } else {
                        System.out.println("GET request not worked");
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();






    }
}
