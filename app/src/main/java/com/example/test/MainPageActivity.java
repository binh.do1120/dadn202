package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import org.json.*;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainPageActivity extends AppCompatActivity {
    private MQTTService mqttService;
    private MQTTService mqttService1;
    private HashMap<Integer, String> deviceTypeMap = new HashMap<>();
    private HashMap<Integer, Switch> switchMap = new HashMap<>();
    String username= "";
    String password ="";

    public void viewHistory(View v) {
        Intent clickIntent = new Intent(getBaseContext(), ViewHistory.class);
        clickIntent.putExtra("EXTRA_USERNAME", username);
        startActivity(clickIntent);
    }

    View.OnClickListener deviceClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent clickIntent = new Intent(getBaseContext(), ControlDeviceView.class);
            clickIntent.putExtra("EXTRA_DEVICE_ID", String.valueOf(v.getId()));
            clickIntent.putExtra("EXTRA_USERNAME", username);
            clickIntent.putExtra("EXTRA_PASSWORD", password);
            startActivity(clickIntent);
        }
    };

    public void addDevice(View v) {
        Intent clickIntent = new Intent(getBaseContext(), ControlDeviceAdd.class);
        clickIntent.putExtra("EXTRA_USERNAME", username);
        startActivity(clickIntent);
    }

    View.OnClickListener deleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout deviceList = findViewById(R.id.linearLayoutDevices);
            int target = (int)v.getId() - 2000;
            for (int i = 0; i < deviceList.getChildCount(); i++) {
                if ((int)deviceList.getChildAt(i).getId() - 1000 == target) {
                    deviceList.removeViewAt(i);
                    break;
                }
            }
            ExecutorService ex = Executors.newSingleThreadExecutor();
            ex.submit(() ->
                    ServerConnection.DeleteDevice(target)
            );

        }
    };

    CompoundButton.OnCheckedChangeListener switchListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String state = "0";
            if (isChecked) state = "1";
            String deviceType = "LED";
            if (deviceTypeMap.get(buttonView.getId()).equals("water")) {
                deviceType = "RELAY";
            }
            String data = "{\"id\": \"" + String.valueOf(buttonView.getId()) + "\", \"name\": \"" + deviceType + "\", \"data\": \"" + state + "\", \"unit\": \"\"}";
            //connection timeout in 10s, 500ms check intervals
            System.out.println("data to send: " + data);
            for (int timeout = 0; timeout < 20; timeout++) {
                if (mqttService1.isConnected()) {
                    if (deviceTypeMap.get(buttonView.getId()).equals("water")) {
                        mqttService1.sendDataMQTT(mqttService1.getUsername() + "/feeds/bk-iot-relay", data);
                    }
                    else {
                        mqttService1.sendDataMQTT(mqttService1.getUsername() + "/feeds/bk-iot-led", data);
                    }
                    break;
                }
                try {
                    Thread.sleep(500);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    MqttCallbackExtended callback = new MqttCallbackExtended() {
        @Override
        public void connectComplete(boolean reconnect, String serverURI) {

        }

        @Override
        public void connectionLost(Throwable cause) {

        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            String data_to_microbit = message.toString();
            JSONObject msg = new JSONObject(data_to_microbit);
            String feed = topic.substring(topic.indexOf('/'));
            if (msg.getString("name").equals("LIGHT")) {
                TextView target = findViewById(R.id.textViewLight);
                target.setText(getResources().getString(R.string.sensor_value_and_unit, msg.getString("data"), msg.getString("unit")));
            }
            if (msg.getString("name").equals("SOIL")) {
                TextView target = findViewById(R.id.textViewHumidity);
                target.setText(getResources().getString(R.string.sensor_value_and_unit, msg.getString("data"), msg.getString("unit")));
            }
            if (msg.getString("name").equals("TEMP-HUMID")) {
                TextView target = findViewById(R.id.textViewTemperature);
                String temp = msg.getString("data").split("-")[0];
                String tempUnit = msg.getString("unit").split("-")[0];
                target.setText(getResources().getString(R.string.sensor_value_and_unit, temp, tempUnit));
            }
            if (msg.getString("name").equals("LED") || msg.getString("name").equals("RELAY")) {
                Switch target = switchMap.get(Integer.parseInt(msg.getString("id")));
                if (target.isChecked() && msg.getString("data").equals("0")) {
                    target.setChecked(false);
                }
                else if (!target.isChecked() && msg.getString("data").equals("1")) {
                    target.setChecked(true);
                }
            }
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main_page);

        boolean mqttInfo = false;

        Intent intent = getIntent();
        username = intent.getStringExtra(MainActivity.EXTRA_USERNAME);
        password = intent.getStringExtra(MainActivity.EXTRA_PASSWORD);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                mqttService = ServerConnection.GetMqttInfo(username, password, this);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            ex.shutdown();
        });
        try {
            ex.awaitTermination(10, TimeUnit.SECONDS);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        mqttService1 = mqttService;
        mqttService.setCallback(callback);
        mqttService1.setCallback(callback);
        mqttService.connect();
        mqttService1.connect();

        ExecutorService ex1 = Executors.newSingleThreadExecutor();
        ex1.submit(() -> {
            //connection timeout in 10s, 500ms check intervals
            for (int timeout = 0; timeout < 20; timeout++) {
                if (mqttService.isConnected() && mqttService1.isConnected()) {
                    //mqttService.subscribeToTopic(mqttService.getUsername() + "/feeds/bk-iot-light");
                    mqttService.subscribeToTopic(mqttService.getUsername() + "/feeds/bk-iot-soil");
                    mqttService.subscribeToTopic(mqttService.getUsername() + "/feeds/bk-iot-temp-humid");
                    mqttService.subscribeToTopic(mqttService.getUsername() + "/feeds/bk-iot-led");
                    //mqttService.subscribeToTopic(mqttService.getUsername() + "/feeds/bk-iot-relay");

                    mqttService1.subscribeToTopic(mqttService1.getUsername() + "/feeds/bk-iot-light");
                    //mqttService1.subscribeToTopic(mqttService1.getUsername() + "/feeds/bk-iot-soil");
                    //mqttService1.subscribeToTopic(mqttService1.getUsername() + "/feeds/bk-iot-temp-humid");
                    mqttService1.subscribeToTopic(mqttService1.getUsername() + "/feeds/bk-iot-relay");
                    break;
                }
                try {
                    Thread.sleep(500);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            String result = "";
            try {
                JSONArray response = ServerConnection.GetDeviceStates(username, password, mqttService1.getUsername(), this);
                runOnUiThread(() -> {
                    try {
                        LinearLayout devices = findViewById(R.id.linearLayoutDevices);
                        devices.removeAllViewsInLayout();
                        deviceTypeMap.clear();
                        switchMap.clear();
                        for (int i = 0; i < response.length(); i++) {
                            int deviceId = response.getJSONObject(i).getInt("id");
                            String deviceName = response.getJSONObject(i).getString("name");
                            String deviceType = response.getJSONObject(i).getString("control_device_type");
                            int deviceState = response.getJSONObject(i).getInt("value");


                            ConstraintLayout row = new ConstraintLayout(this);
                            devices.addView(row);

                            Switch device = new Switch(this);
                            ImageButton deleteButton = new ImageButton(this);
                            deleteButton.setLayoutParams(new LinearLayout.LayoutParams(
                                    100,
                                    100)
                            );
                            deleteButton.setBackground(getResources().getDrawable(android.R.drawable.ic_menu_delete));
                            deleteButton.setId(deviceId + 2000);
                            deleteButton.setOnClickListener(deleteClickListener);
                            ImageButton editButton = new ImageButton(this);

                            row.setLayoutParams(new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    100)
                            );
                            row.setId(deviceId + 1000);

                            editButton.setLayoutParams(new LinearLayout.LayoutParams(
                                    100,
                                    100)
                            );
                            editButton.setBackground(getResources().getDrawable(android.R.drawable.ic_menu_info_details));
                            editButton.setId(deviceId);
                            editButton.setOnClickListener(deviceClickListener);
                            device.setText(deviceName);
                            device.setId(deviceId);
                            device.setChecked(deviceState == 1);
                            device.setOnCheckedChangeListener(switchListener);
                            device.setTextSize(20);
                            switchMap.put(deviceId, device);
                            deviceTypeMap.put(deviceId, deviceType);

                            row.addView(editButton);
                            row.addView(device);
                            row.addView(deleteButton);
                            ((ConstraintLayout.LayoutParams) deleteButton.getLayoutParams()).leftToLeft = deviceId + 1000;
                            ((ConstraintLayout.LayoutParams) editButton.getLayoutParams()).leftToRight = deviceId + 2000;
                            ((ConstraintLayout.LayoutParams) device.getLayoutParams()).rightToRight = deviceId + 1000;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}