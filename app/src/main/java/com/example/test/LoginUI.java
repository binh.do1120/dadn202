package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.service.controls.Control;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LoginUI extends AppCompatActivity {
    public static final String EXTRA_USERNAME = "com.example.test.USERNAME";
    public static final String EXTRA_PASSWORD = "com.example.test.PASSWORD";
    ImageView loadingIcon;
    Button loginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login_ui);
        loadingIcon = (ImageView) findViewById(R.id.LoadingIcon);
        loadingIcon.setVisibility(View.GONE);
        loginButton = (Button) findViewById(R.id.LoginButton);
    }

    public void StartLoginCheck()
    {
        loginButton.setClickable(false);
        loadingIcon.setVisibility(View.VISIBLE);
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setDuration(1000);
        rotate.setInterpolator(new LinearInterpolator());
        loadingIcon.startAnimation(rotate);
    }

    public void StopLoginCheck()
    {
        loadingIcon.clearAnimation();
        loadingIcon.setVisibility(View.GONE);
    }

    public void OnLoginClick(View v) throws InterruptedException {
        final boolean[] valid = {false};
        StartLoginCheck();
        String username = ((EditText)findViewById(R.id.UsernameEditText)).getText().toString();
        String password = ((EditText)findViewById(R.id.PasswordEditText)).getText().toString();
        Intent intent = new Intent(this, MainPageActivity.class);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                valid[0] = ServerConnection.AuthenticateUser(username,password);
                runOnUiThread(() ->
                        StopLoginCheck()
                );
                loginButton.setClickable(true);
                System.out.println("Result: " + valid[0]);
                if(valid[0]) {
                    System.out.println("Login Successful");
                    intent.putExtra(EXTRA_USERNAME, username);
                    intent.putExtra(EXTRA_PASSWORD, password);
                    startActivity(intent);
                }
                else {
                    System.out.println("Login failed");
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Wrong username or password", Toast.LENGTH_LONG).show()
                    );
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

    }

}
