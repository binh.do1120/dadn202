package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ViewHistory extends AppCompatActivity {
    LinearLayout linearLayoutHistory;
    Spinner spinner3;
    private ArrayList<Sensor> sensorList = new ArrayList<Sensor>();
    private ArrayAdapter<Sensor> sensorListAdapter;
    String username = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_view_history);
        linearLayoutHistory = findViewById(R.id.linearLayoutHistory);

        Intent intent = getIntent();
        username = intent.getStringExtra("EXTRA_USERNAME");
        spinner3 = findViewById(R.id.spinner3);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                JSONArray response = ServerConnection.GetSensors(username, this);
                for (int i = 0; i < response.length(); i++) {
                    Sensor s = new Sensor(response.getJSONObject(i).getString("sensor_type"), response.getJSONObject(i).getString("id"));
                    sensorList.add(s);
                }
                ex.shutdown();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        });
        try {
            ex.awaitTermination(10, TimeUnit.SECONDS);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        sensorListAdapter = new ArrayAdapter<Sensor>(this, R.layout.support_simple_spinner_dropdown_item, sensorList);
        spinner3.setAdapter(sensorListAdapter);

        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Sensor s = (Sensor)parent.getItemAtPosition(position);
                ExecutorService ex = Executors.newSingleThreadExecutor();
                ex.submit(() -> {
                    try {
                        JSONArray response = ServerConnection.GetSensorStats(s.id, s.type);
                        runOnUiThread(() -> {
                            try {
                                linearLayoutHistory.removeAllViewsInLayout();
                                for (int i = 0; i < response.length(); i++) {
                                    TextView textViewData = new TextView(view.getContext());
                                    textViewData.setLayoutParams(new ConstraintLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT)
                                    );
                                    textViewData.setTextSize(20);
                                    TextView textViewTime = new TextView(view.getContext());
                                    textViewTime.setLayoutParams(new ConstraintLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT)
                                    );
                                    textViewTime.setTextSize(20);
                                    ConstraintLayout row = new ConstraintLayout(view.getContext());
                                    row.setLayoutParams(new ConstraintLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT,
                                            100)
                                    );
                                    linearLayoutHistory.addView(row);
                                    row.setId(i);
                                    row.addView(textViewData);
                                    row.addView(textViewTime);
                                    ((ConstraintLayout.LayoutParams)textViewData.getLayoutParams()).leftToLeft = i;
                                    ((ConstraintLayout.LayoutParams)textViewTime.getLayoutParams()).rightToRight = i;

                                    String dataText = "";
                                    if (s.type.equals("TEMP-HUMID")) {
                                        dataText += response.getJSONObject(i).getString("value").split("-")[0];
                                        dataText += response.getJSONObject(i).getString("unit").split("-")[0];
                                    }
                                    else {
                                        dataText += response.getJSONObject(i).getString("value");
                                        dataText += response.getJSONObject(i).getString("unit");
                                    }
                                    textViewData.setText(dataText);
                                    textViewTime.setText(response.getJSONObject(i).getString("time"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}