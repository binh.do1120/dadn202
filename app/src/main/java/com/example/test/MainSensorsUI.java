package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainSensorsUI extends AppCompatActivity {
    SharedPreferences mPrefs;
    String username;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sensors_ui);

        mPrefs = getSharedPreferences("dadn_storage", 0);
        ctx = getApplicationContext();
        username = mPrefs.getString("user","UserNotFound");

        TextView userFullNameText = (TextView) findViewById(R.id.UserFullNameText);
        userFullNameText.setText(ServerConnection.GetUserFullname(username));
        try {
            AddSensors();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void AddSensors() throws JSONException {
        ConstraintLayout sensorsLayout = (ConstraintLayout) findViewById(R.id.MainSensorsUIBody);
        Integer curViewId = null;
        JSONArray sensorsInfoList = ServerConnection.GetSensorsInfoBriefs(username);
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Integer sensorsBriefsCount =  sensorsInfoList.length()-1;
        for(Integer i = 0; i<=sensorsBriefsCount-1; i++)
        {
            View v = inflater.inflate(R.layout.dashboard_sensor_layout, null);
            Integer horizontalMargin = 20;
            ConstraintSet constraintSet = new ConstraintSet();
            sensorsLayout.addView(
                    v,
                    0,
                    new ConstraintLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    )
            );
            Integer viewId = View.generateViewId();
            v.setId(viewId);

            JSONObject curObj = sensorsInfoList.getJSONObject(i);

            TextView sensorIdText = (TextView) v.findViewById(R.id.SensorsTitle);
            TextView sensorTypeText =   (TextView)v.findViewById(R.id.SensorTypeText);
            TextView sensorTimeText =  (TextView) v.findViewById(R.id.SensorLatestRecordedTimeText);
            TextView sensorValueText = (TextView)  v.findViewById(R.id.SensorValueText);
            TextView sensorUnitText =  (TextView) v.findViewById(R.id.SensorUnitText);

            sensorIdText.setText("ID: " + curObj.getInt("id"));
            sensorTypeText.setText("Type: " + curObj.getString("sensor_type"));
            if(curObj.has("value"))
            {
                sensorTimeText.setText("Last recorded: " + curObj.getString("time"));
                sensorValueText.setText("Value: " + curObj.getString("value"));
                sensorUnitText.setText("unit: " + curObj.getString("unit"));
            }
            else
            {
                sensorTimeText.setText("Last recorded: None");
                sensorValueText.setText("Value: None");
                sensorUnitText.setText("unit: None");
            }
//            if(curObj.getString("sensor_type") == "light")
//            {
//                sensorIdText;
//            }
//            else if (curObj.getString("sensor_type") == "water")
//            {
//                sensorIdText.setTextAppearance(R.style.DashboardContainerInfoWaterStyle);
//            }
            constraintSet.clone(sensorsLayout);
            constraintSet.connect(viewId,ConstraintSet.LEFT,R.id.MainSensorsUIBody,ConstraintSet.LEFT,horizontalMargin);
            constraintSet.connect(viewId,ConstraintSet.RIGHT,R.id.MainSensorsUIBody,ConstraintSet.RIGHT,horizontalMargin);
            if(i==0)
            {
                constraintSet.connect(viewId,ConstraintSet.TOP,R.id.SensorsUITitleText,ConstraintSet.BOTTOM,25);
            }
            else
            {
                constraintSet.connect(viewId,ConstraintSet.TOP,curViewId,ConstraintSet.BOTTOM,15);
            }
            curViewId = viewId;
            constraintSet.applyTo(sensorsLayout);
        }
    }

    public void GoToInsertSensorsUI(View v)
    {
        Intent intent = new Intent(this, InsertSensorUI.class);
        startActivity(intent);
    }

    public void GoToMainDashBoardUI(View v)
    {
        Intent intent = new Intent(this, MainDashboardUI.class);
        startActivity(intent);
    }
}
