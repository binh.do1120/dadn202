package com.example.test;
import android.content.Context;
import android.util.Log;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.Serializable;
import java.nio.charset.Charset;

public class MQTTService implements Serializable {
    final String serverUri ="tcp://io.adafruit.com:1883";
    private String clientId ="testconnection";
    //private String subscriptionTopic ="binhdo1120/feeds/bbc-led";
    private String username ="";
    private String password ="";
    public boolean isConnected = false;
    public MqttAndroidClient mqttAndroidClient;

    public MQTTService(Context context){
        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete( boolean b, String s) {
                Log.w("mqtt", s);
                isConnected = true;
            }
            @Override
            public void connectionLost(Throwable throwable) {
                isConnected = false;
            }
            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Mqtt", mqttMessage.toString());
            }
            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    public String getUsername() { return username;}
    public String getPassword() { return password;}

    public MQTTService username(String username) {
        //adafruit io: your adafruit io account username
        if (username.length() > 0) this.username = username;
        return this;
    }

    public MQTTService password(String password) {
        //adafruit io: your adafruit io dashboard key
        if (password.length() > 0) this.password = password;
        return this;
    }

    public void unsubscribe(String topic) {
        try {
            mqttAndroidClient.unsubscribe(topic);
        }
        catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    public MQTTService clientId(String clientId) {
        //any string, should describe what this connection is used for
        if (clientId.length() > 0) this.clientId = clientId;
        return this;
    }

    public void setCallback( MqttCallbackExtended callback) {
        mqttAndroidClient.setCallback(callback);
    }

    public void sendDataMQTT(String topic, String data) {
        System.out.print("senDataMQTT running, data = " + data);
        MqttMessage msg = new MqttMessage();
        msg.setId(1234);
        msg.setQos(0);
        msg.setRetained(true);
        byte[] b = data.getBytes(Charset.forName("UTF-8"));
        msg.setPayload(b);
        Log.d("Mqtt","Publish:"+ msg + "to: " + topic);
        try {
            this.mqttAndroidClient.publish(topic, msg);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public void connect(IMqttActionListener iMqttActionListener) {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, iMqttActionListener);
        }
        catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    public void connect() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    isConnected = true;
                    //subscribeToTopic();
                }
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt","Failed to connect to:" + serverUri + exception.toString());
                    if (exception.toString().contains(("is connected"))) {
                        isConnected = true;
                    }
                }
            });
        }
        catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    public void subscribeToTopic(String topic) {
        try {
            mqttAndroidClient.subscribe(topic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("Mqtt","Subscribed to " + topic);
                }
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt","Subscribed fail!");
                }
            });
        }
        catch (MqttException ex) {
            System.err.println("Exceptionstsubscribing");
            ex.printStackTrace();
        }
    }
}
