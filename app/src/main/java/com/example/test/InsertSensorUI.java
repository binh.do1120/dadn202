package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class InsertSensorUI extends AppCompatActivity {
    SharedPreferences mPrefs;
    String username;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_sensor_ui);

        mPrefs = getSharedPreferences("dadn_storage", 0);
        ctx = getApplicationContext();
        username = mPrefs.getString("user","UserNotFound");

        TextView userFullNameText = (TextView) findViewById(R.id.UserFullNameText);
        userFullNameText.setText(ServerConnection.GetUserFullname(username));
        try {
            AddFeedUsernameDropdownList();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AddSensorTypeList();
    }

    public void Insert(View v) throws MalformedURLException {
        final Integer[] valid = {500};
        String feedUsername = "";
        String feedName = "";
        String sensorType = "";

        if(((Spinner)findViewById(R.id.MqttFeedUsernameDropdown)).getSelectedItem() != null);
        {
            feedUsername = ((Spinner)findViewById(R.id.MqttFeedUsernameDropdown)).getSelectedItem().toString();
        }
        if(((Spinner)findViewById(R.id.MqttFeedNameDropdown)).getSelectedItem() != null);
        {
            System.out.println("asasdasd");
            feedName = ((Spinner) findViewById(R.id.MqttFeedNameDropdown)).getSelectedItem().toString();
        }
        if(((Spinner)findViewById(R.id.SensorTypeDropdown)).getSelectedItem() != null);
        {
            sensorType = ((Spinner)findViewById(R.id.SensorTypeDropdown)).getSelectedItem().toString();
        }

        Button insertButton = (Button) findViewById(R.id.InsertSensorButton);
        insertButton.setClickable(false);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        String finalFeedUsername = feedUsername;
        String finalFeedName = feedName;
        String finalSensorType = sensorType;
        ex.submit(() -> {
            try {
                valid[0] = ServerConnection.InsertSensor(username, finalFeedUsername, finalFeedName, finalSensorType);
                System.out.println("Result: " + valid[0]);
                if(valid[0] == HttpURLConnection.HTTP_OK) {
                    GoBack();
                }
                else {
                    insertButton.setClickable(true);
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Failed to add sensor", Toast.LENGTH_LONG).show()
                    );
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        });

    }

    public void GoBack(View v)
    {
        GoBack();
    }

    public void GoBack()
    {
        Intent intent = new Intent(this, MainSensorsUI.class);
        startActivity(intent);
    }


    void AddFeedUsernameDropdownList() throws JSONException {
        ArrayList<String> usernameList = new ArrayList<String>();
        JSONArray feedUsernameList = ServerConnection.GetUserMqttFeedUsernames(username);
        for (Integer i = 0; i<feedUsernameList.length();i++)
        {
            usernameList.add(feedUsernameList.getJSONObject(i).getString("feed_username"));
        }
        if(feedUsernameList.length() == 0)
        {
            usernameList.add("");
        }
        Spinner feedUsernameSpinner = (Spinner) findViewById(R.id.MqttFeedUsernameDropdown);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, usernameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feedUsernameSpinner.setAdapter(adapter);
        feedUsernameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                try {
                    AddFeedNamesDropdownList(username,usernameList.get(position));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    void AddFeedNamesDropdownList(String owner, String feedUsername) throws JSONException {
        ArrayList<String> feedNameList = new ArrayList<String>();
        JSONArray feedUsernameList = ServerConnection.GetFeedNamesList(owner,feedUsername);
        for (Integer i = 0; i<feedUsernameList.length();i++)
        {
            feedNameList.add(feedUsernameList.getJSONObject(i).getString("feed_name"));
        }
        if(feedUsernameList.length() == 0)
        {
            feedNameList.add("");
        }
        Spinner feednameSpinner = (Spinner) findViewById(R.id.MqttFeedNameDropdown);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, feedNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feednameSpinner.setAdapter(adapter);
    }

    void AddSensorTypeList()
    {
        ArrayList<String> sensorTypeList = new ArrayList<String>();
        Spinner sensorTypeSpinner = (Spinner) findViewById(R.id.SensorTypeDropdown);
        sensorTypeList.add("light");
        sensorTypeList.add("water");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sensorTypeList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sensorTypeSpinner.setAdapter(adapter);
    }
}