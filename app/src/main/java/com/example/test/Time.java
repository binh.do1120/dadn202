package com.example.test;

import java.util.Calendar;

public class Time {
    Integer year;
    Integer month;
    Integer date;
    Integer hour;
    Integer minute;
    Boolean isNull;
    public Boolean IsNull()
    {
        return isNull;
    }

    public Time(Calendar cal)
    {
        this(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
    }

    public Time(Integer year, Integer month, Integer date, Integer hour, Integer minute) {
        this.year = year;
        this.month = month+1;
        this.date = date;
        this.hour = hour;
        this.minute = minute;
        isNull = false;
    }

    public Time()
    {
        isNull = true;
    }

    @Override
    public String toString() {
        String res = "";
        res += this.year + "-";
        res += this.month + "-";
        res += this.date;
        res += "T";
        res += this.hour + ":";
        res += this.minute;
        res += "Z";
        return res;
    }
}

