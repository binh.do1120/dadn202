package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ControlDeviceView extends AppCompatActivity {
    private String username ="";
    private String password = "";
    private String controlDeviceId = "";

    public void editButtonListener(View v) {
        Intent clickIntent = new Intent(getBaseContext(), ControlDeviceConfig.class);
        clickIntent.putExtra("EXTRA_DEVICE_ID", controlDeviceId);
        clickIntent.putExtra("EXTRA_USERNAME", username);
        clickIntent.putExtra("EXTRA_PASSWORD", password);
        startActivity(clickIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_control_device_view);

        Intent intent = getIntent();
        username = intent.getStringExtra("EXTRA_USERNAME");
        password = intent.getStringExtra("EXTRA_PASSWORD");
        controlDeviceId = intent.getStringExtra("EXTRA_DEVICE_ID");


    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView deviceName = findViewById(R.id.textViewDeviceName);
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(() -> {
            try {
                JSONArray response = ServerConnection.GetControlDeviceInfo(username, password, controlDeviceId, this);
                runOnUiThread(() -> {
                    try {
                        TextView sensorType = findViewById(R.id.textViewSensorType);
                        TextView sensorID = findViewById(R.id.textViewSensorID);
                        TextView sensorMin = findViewById(R.id.textViewSensorMin);
                        TextView sensorMax = findViewById(R.id.textViewSensorMax);
                        sensorID.setText(controlDeviceId);
                        sensorType.setText(response.getJSONObject(0).getString("control_device_type"));
                        sensorMin.setText(String.valueOf(response.getJSONObject(0).getDouble("lowest_stats")));
                        sensorMax.setText(String.valueOf(response.getJSONObject(0).getDouble("highest_stats")));
                        JSONArray timeList = response.getJSONObject(0).getJSONArray("time");
                        LinearLayout timers = findViewById(R.id.linearLayoutTimeList);
                        timers.removeAllViewsInLayout();
                        for (int i = 0; i < timeList.length(); i++) {
                            int hourStart = timeList.getJSONObject(i).getInt("start_time") / 3600;
                            int minuteStart = (timeList.getJSONObject(i).getInt("start_time") / 60) % 60;
                            int secondStart = timeList.getJSONObject(i).getInt("start_time") % 60;
                            int hourEnd = timeList.getJSONObject(i).getInt("end_time") / 3600;
                            int minuteEnd = (timeList.getJSONObject(i).getInt("end_time") / 60) % 60;
                            int secondEnd = timeList.getJSONObject(i).getInt("end_time") % 60;

                            TextView row = new TextView(this);
                            row.setLayoutParams(new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT)
                            );
                            String rowText = String.valueOf(hourStart);
                            rowText += ":" + String.valueOf(minuteStart);
                            rowText += ":" + String.valueOf(secondStart);
                            rowText += " to " + String.valueOf(hourEnd);
                            rowText += ":" + String.valueOf(minuteEnd);
                            rowText += ":" + String.valueOf(secondEnd);
                            row.setText(rowText);
                            row.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);

                            timers.addView(row);
                        }
                        deviceName.setText(response.getJSONObject(0).getString("name"));
                    }
                    catch (Exception e) {
                        deviceName.setText("Unnamed");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}