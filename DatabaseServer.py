import socket
import http.server
import threading
import mysql.connector
import json
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime
import requests
from time import sleep
import numpy as np
from flask import Flask
from flask import request

app = Flask(__name__)
PORT = 8080
mqttClient = {}
db = None
MqttNameList = dict()
MqttNameList['1'] = "LED"
MqttNameList['2'] = "SPEAKER"
MqttNameList['3'] = "LCD"
MqttNameList['4'] = "BUTTON"
MqttNameList['5'] = "TOUCH"
MqttNameList['6'] = "TRAFFIC"
MqttNameList['7'] = "TEMP-HUMID"
MqttNameList['8'] = "MAGNETIC"
MqttNameList['9'] = "SOIL"
MqttNameList['10'] = "DRV_PWM"
MqttNameList['11'] = "RELAY"
MqttNameList['12'] = "SOUND"
MqttNameList['13'] = "LIGHT"
MqttNameList['16'] = "INFRARED"
MqttNameList['17'] = "SERVO"
MqttNameList['22'] = "TIME"
MqttNameList['23'] = "GAS"

MqttUnitList = dict()
for i in range(0,24):
    MqttUnitList[str(i)]=""
MqttUnitList["17"]="degree"
MqttUnitList["7"]="*C-%"

mysqlHost="localhost"
mysqlUser="root"
mysqlPassword="binh"
mysqlDatabase="dadn"

# Returns the json of the payload
def CustomPayload(id, data, name):
    payload = dict()
    payload["id"]=str(id)
    payload["name"]=str(name)
    payload["data"]=str(data)
    payload["unit"]=str(MqttUnitList[str(id)])
# {"id": "1", "name": "LED", "data": "0", "unit": ""}
    res = "{\"id\":\"" + payload["id"] + "\",\"name\":\""
    res += payload["name"] + "\",\"data\":\""
    res += payload["data"] + "\",\"unit\":\""
    res += payload["unit"] + "\"}"
    return res


def on_connect(client, userdata, flags, rc):
    print("Connected with result code: "+str(rc))

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")

def on_message(client, userdata, msg): 
    data = json.loads(msg.payload)
    mqtt_username = msg.topic.split('/')[0]
    mqtt_feed_name = msg.topic.split('/')[2]
    cursor = db.cursor(dictionary=True)

    # Get the sensorid
    mysqlGetSensorIdQuery = '''SELECT id, sensor_type
            FROM sensor
            WHERE id = %s AND sensor_type = %s'''
    cursor.execute(mysqlGetSensorIdQuery, (data["id"], data["name"]))
    result = cursor.fetchall()
    #attempt to add sensor to database if it doesnt exist
    if len(result) == 0:
        #attempt to find username from mqtt info
        cursor.execute('''
            SELECT username FROM sensor WHERE feed_username=%s
        ''', (mqtt_username,))
        result = cursor.fetchall()
        #do nothing if cant find
        if len(result) == 0:
            return
        cursor.execute('''
        INSERT INTO sensor (id, feed_username, feed_name, username, sensor_type) VALUES (%s, %s, %s, %s, %s)
        ''', (data["id"], mqtt_username, mqtt_feed_name, result[0]["username"], data["name"]))
    sensorId = data['id']
    sensorType = data['name']
    inputData = data["data"]
    inputUnit = data['unit']

    # Insert the new feed response to the mysql database
    now = datetime.now()
    cursor.execute('''INSERT INTO sensor_stat (sensor_id, time, sensor_type, value, unit) 
        VALUES (%s, %s, %s, %s, %s)''', 
        (sensorId, now.strftime('%Y-%m-%d %H:%M:%S'), sensorType, inputData, inputUnit))
    db.commit()

    if isinstance(inputData, str) and "-" in inputData:
        inputData = inputData.split("-")[0]

    # Get the mqtt feed info for the control devices that are going to be enabled
    mysqlGetMqttEnableControlDeviceQuery = '''SELECT mf.username, mf.feed_name, mf.feed_key, cd.control_device_type, cd.id
        FROM control_device AS cd, mqtt_feed AS mf
        WHERE sensor_id IN
            (SELECT sensor.id
            FROM sensor
            WHERE sensor.feed_username = %s AND sensor.feed_name = %s)
            AND cd.feed_username = mf.username AND cd.feed_name = mf.feed_name
            AND cd.user_enabled = 1 AND cd.id IN (
                SELECT DISTINCT cdt.control_device_id
                FROM control_device_time AS cdt
                WHERE 
                    (start_time < end_time AND NOW() BETWEEN start_time AND end_time)
                    OR
                    (end_time < start_time AND NOW() < start_time AND NOW() < end_time)
                    OR
                    (end_time < start_time AND NOW() > start_time)
            )
            AND ''' + inputData +''' BETWEEN cd.lowest_stats AND cd.highest_stats

    '''
    cursor.execute(mysqlGetMqttEnableControlDeviceQuery, (mqtt_username,mqtt_feed_name))
    mqttEnableControlDeviceFeedInfoList = cursor.fetchall()
    for mqttEnableInfo in mqttEnableControlDeviceFeedInfoList:
        EnableControlDevice(
            feed_username=mqttEnableInfo["username"],
            feed_key=mqttEnableInfo["feed_key"],
            feed_name=mqttEnableInfo["feed_name"],
            control_device_type=mqttEnableInfo["control_device_type"],
            control_device_id=mqttEnableInfo["id"]
        )

    # Get the mqtt feed info for the control devices that are going to be enabled
    mysqlGetMqttDisableControlDeviceQuery = '''SELECT mf.username, mf.feed_name, mf.feed_key, cd.control_device_type, cd.id
    FROM control_device AS cd, mqtt_feed AS mf
    WHERE sensor_id IN
        (SELECT sensor.id
        FROM sensor
        WHERE sensor.feed_username = %s AND sensor.feed_name = %s)
        AND cd.feed_username = mf.username AND cd.feed_name = mf.feed_name
        AND cd.user_enabled = 1 AND cd.id IN (
            SELECT DISTINCT cdt.control_device_id
            FROM control_device_time AS cdt
            WHERE 
                (start_time < end_time AND NOW() BETWEEN start_time AND end_time)
                OR
                (end_time < start_time AND NOW() < start_time AND NOW() < end_time)
                OR
                (end_time < start_time AND NOW() > start_time)
        )
        AND NOT (''' + inputData +''' BETWEEN cd.lowest_stats AND cd.highest_stats)

    '''
    cursor.execute(mysqlGetMqttDisableControlDeviceQuery, (mqtt_username,mqtt_feed_name))
    mqttDisableControlDeviceFeedInfoList = cursor.fetchall()
    for mqttDisableInfo in mqttDisableControlDeviceFeedInfoList:
        DisableControlDevice(
            feed_username=mqttDisableInfo["username"],
            feed_key=mqttDisableInfo["feed_key"],
            feed_name=mqttDisableInfo["feed_name"],
            control_device_type=mqttDisableInfo["control_device_type"],
            control_device_id=mqttDisableInfo["id"]
        )


class myHandler(http.server.BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def _html(self, message):
        content = f"<html><body><h1>{message}</h1></body></html>"
        return content.encode("utf8")  # NOTE: must return a bytes object!

    def do_GET(self):
        self._set_headers()
        separator_index = self.path.find("?")
        if separator_index != -1:
            request = self.path[:separator_index]
            param_list = self.path[separator_index + 1:].split("&")
        else:
            request = self.path
            param_list = []
        params = {}
        for pair in param_list:
            new_pair = pair.replace("+", " ").replace("%5B", "[").replace("%5D", "]").replace("%7B", "{").replace("%7D", "}").replace("%3A", ":").replace("%22", "\"").replace("%2C", ",")
            key_value = new_pair.split("=")
            params[key_value[0]] = key_value[1]

        if request =='/get_device_states':
            cursor = db.cursor(dictionary = True)
            cursor.execute('''
                SELECT id, control_device_type, name FROM control_device
                WHERE feed_username = %s
            ''', (params["mqtt_name"], ))
            result = cursor.fetchall() #list of (id, type, name, state)
            for item in result:
                sensor_type = "RELAY" if item["control_device_type"] == "water" else "LED"
                cursor.execute('''
                    SELECT value FROM sensor_stat 
                    WHERE sensor_type = %s AND time = (
                        SELECT MAX(time) FROM sensor_stat
                        WHERE sensor_type = %s AND sensor_id = %s
                    )
                ''', (sensor_type, sensor_type, item["id"]))
                data = cursor.fetchall()
                if data:
                    item["value"] = data[0]["value"]
                else:
                    item["value"] = "0"

            self.wfile.write(json.dumps(result).encode("utf8"))

        elif request == '/get_sensor_stats':
            cursor = db.cursor(dictionary = True)
            cursor.execute('''
                SELECT value, unit, time FROM sensor_stat WHERE sensor_id=%s AND sensor_type=%s ORDER BY time
            ''', (params["sensor_id"], params["sensor_type"]))
            result = cursor.fetchall()
            response = []
            for item in result:
                time_string = ""
                time_string += str(item["time"].year) + "/"
                time_string += str(item["time"].month) + "/"
                time_string += str(item["time"].day) + " - "
                time_string += str(item["time"].hour) + ":"
                time_string += str(item["time"].minute) + ":"
                time_string += str(item["time"].second)
                item["time"] = time_string
                response.append(item)
            self.wfile.write(json.dumps(response).encode("utf8"))

        elif request == '/add_control_device':
            cursor = db.cursor(dictionary = True)
            feed_name = "bk-iot-relay"
            if params["type"] == "light":
                feed_name = "bk-iot-led"
            cursor.execute('''
                SELECT username FROM mqtt_feed WHERE feed_name=%s
            ''', (feed_name,))
            feed_username = cursor.fetchall()[0]["username"]
            cursor.execute('''
                INSERT IGNORE INTO control_device (id, name, sensor_id, sensor_type, control_device_type, lowest_stats, highest_stats, feed_username, feed_name, user_enabled)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            ''',(params["control_device_id"], params["name"], params["sensor_id"], params["sensor_type"], params["type"], params["lowest_stats"], params["highest_stats"], feed_username, feed_name, params["user_enabled"]))
            db.commit()
            self.wfile.write("success".encode("utf8"))

        elif request == '/delete_device':
            cursor = db.cursor(dictionary = True)
            cursor.execute('''
                DELETE FROM control_device WHERE id=%s
            ''', (params["control_device_id"],))
            cursor.execute('''
                DELETE IGNORE FROM sensor WHERE id=%s AND (sensor_type=%s OR sensor_type=%s)
            ''', (params["control_device_id"], "LED", "RELAY"))
            db.commit()
            self.wfile.write("success".encode("utf8"))

        elif request == '/get_mqtt_info':
            cursor = db.cursor(dictionary = True)
            cursor.execute('''
                SELECT username, feed_key FROM mqtt_feed
                WHERE username IN (
                    SELECT feed_username FROM sensor
                    WHERE username = %s
                )
            ''', (params["username"],))
            result = cursor.fetchall()
            self.wfile.write(json.dumps(result).encode("utf8"))

        elif request == '/auth':
            cursor = db.cursor()
            cursor.execute("SELECT username FROM user WHERE username = %s AND password = %s", (params["username"], params["password"]))
            result = cursor.fetchall()
            if result:
                self.wfile.write("valid".encode("utf8"))
            else:
                self.wfile.write("invalid".encode("utf8"))

        elif request == '/get_control_device_info':
            cursor = db.cursor(dictionary = True)
            cursor.execute("SELECT sensor_id, sensor_type, control_device_type, lowest_stats, highest_stats, name, user_enabled FROM control_device WHERE id = %s", (params["control_device_id"],))
            response = cursor.fetchall()
            cursor.execute("SELECT start_time, end_time FROM control_device_time WHERE control_device_id = %s", (params["control_device_id"],))
            result = cursor.fetchall()
            time_list = []
            for row in result:
                time_list.append({k: v.seconds for k, v in row.items()})
            response[0]["time"] = time_list
            self.wfile.write(json.dumps(response).encode("utf8"))

        elif request == '/update_control_device':
            print(params["timer_list"])
            timer_list = json.loads(params["timer_list"])
            cursor = db.cursor(dictionary = True)
            cursor.execute('''
                DELETE FROM control_device_time WHERE control_device_id=%s
            ''', (params["control_device_id"],))
            for time in timer_list:
                cursor.execute('''
                    INSERT IGNORE INTO control_device_time (control_device_id, start_time, end_time) VALUES (%s, %s, %s)
                ''', (params["control_device_id"], time["start_time"], time["end_time"]))
            cursor.execute('''
                UPDATE IGNORE control_device SET name=%s, sensor_id=%s, sensor_type=%s, lowest_stats=%s, highest_stats=%s, user_enabled=%s, id=%s, control_device_type=%s
                WHERE id=%s
            ''', (params["name"], params["sensor_id"], params["sensor_type"], params["lowest_stats"], params["highest_stats"], params["user_enabled"], params["device_id"], params["type"], params["control_device_id"]))
            if params["type"] == "water":
                cursor.execute('''
                    UPDATE IGNORE control_device SET feed_name=%s WHERE id=%s
                ''', ("bk-iot-relay", params["control_device_id"]))
            else:
                cursor.execute('''
                    UPDATE IGNORE control_device SET feed_name=%s WHERE id=%s
                ''', ("bk-iot-led", params["control_device_id"]))
            db.commit()
            self.wfile.write("success".encode("utf8"))

        elif request == '/get_sensors':
            cursor = db.cursor(dictionary = True)
            cursor.execute("SELECT sensor_type, id FROM sensor WHERE username=%s", (params["username"],))
            result = cursor.fetchall()
            self.wfile.write(json.dumps(result).encode("utf8"))

        else:
            self.send_response(404)
            self.send_header("Content-type", "text/html")
            self.end_headers()

    def do_POST(self):
        self._set_headers()
        pass


# Run this Http server
def runHttpServer(server_class=http.server.HTTPServer, handler_class=myHandler):
    server_address = ('', PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


# Fire all the threads to wait for sensors input
def mqttLoop(mqtt_info):
    client = mqtt.Client()
    client.username_pw_set(mqtt_info['username'], mqtt_info['feed_key'])
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message
    client.connect("io.adafruit.com")
    client.subscribe(mqtt_info['username'] + "/feeds/" + mqtt_info['feed_name'])    
    client.loop_forever()


# Create all listeners for all of the sensors
def RunMqttSensorsListeners():
    for item in GetAllMqttSensorsFeeds():
        threading.Thread(target=mqttLoop, args=(item, )).start()


# Gets all the mqtt sensors feeds in the database
def GetAllMqttSensorsFeeds():
    cursor = db.cursor(dictionary = True)
    mysqlMqttGetFeedQuery = "SELECT username, feed_name, feed_key FROM mqtt_feed"
    cursor.execute(mysqlMqttGetFeedQuery)
    mqttFeedList = cursor.fetchall()
    return mqttFeedList


# Turn a control device on
def EnableControlDevice(feed_username, feed_key,feed_name, control_device_type, control_device_id):
    auth = {}
    auth["username"] = str(feed_username)
    auth["password"] = str(feed_key)
    payload=""
    if control_device_type == "water":
        payload = CustomPayload(control_device_id, 1, "RELAY")
    elif control_device_type == "light":
       payload = CustomPayload(control_device_id, 2, "LED")
    publish.single(topic=feed_username+"/feeds/"+feed_name, payload=payload, qos=0, retain=False, hostname="io.adafruit.com", auth=auth)


# Turn a control device off
def DisableControlDevice(feed_username, feed_key, feed_name, control_device_type, control_device_id):
    auth = {}
    auth["username"] = str(feed_username)
    auth["password"] = str(feed_key)
    payload=""
    if control_device_type == "water":
        payload = CustomPayload(control_device_id, 0, "RELAY")
    elif control_device_type == "light":
        payload = CustomPayload(control_device_id, 0, "LED")
    publish.single(topic=feed_username+"/feeds/"+feed_name, payload=payload, qos=0, retain=False, hostname="io.adafruit.com", auth=auth)


# Disable the control devices that is not within their work time
def SetControlDeviceDisableLoop():
    db = ConnectDatabase()
    cursor = db.cursor(dictionary = True)
    while True:
        mysqlGetMqttDisableControlDevicesQuery =  '''
            SELECT DISTINCT mf.feed_key, mf.username, mf.feed_name, cd.control_device_type, cd.id
            FROM mqtt_feed AS mf, control_device AS cd
            WHERE cd.id NOT IN   (
                SELECT DISTINCT cdt.control_device_id
                FROM control_device_time AS cdt
                WHERE 
                    (start_time < end_time AND NOW() BETWEEN start_time AND end_time)
                    OR
                    (end_time < start_time AND NOW() < start_time AND NOW() < end_time)
                    OR
                    (end_time < start_time AND NOW() > start_time)
                )
                AND mf.username = cd.feed_username AND mf.feed_name = cd.feed_name
        '''
        cursor.execute(mysqlGetMqttDisableControlDevicesQuery)
        disabledMqttInfoList = cursor.fetchall()
        for disablingInfo in disabledMqttInfoList:
            feed_username = disablingInfo["username"]
            feed_key = disablingInfo["feed_key"]
            feed_name = disablingInfo["feed_name"]
            device_type = disablingInfo["control_device_type"]
            device_id = disablingInfo["id"]
            DisableControlDevice(
                feed_username=feed_username,
                feed_key=feed_key,
                feed_name=feed_name,
                control_device_type=device_type,
                control_device_id=device_id
            )

        print("60 seconds has passed")
        sleep(60)

        # print("Disabling: " +disabledMqttInfoList.__str__())


######### Python Flask Server's APIs for port 5000 ############
# @app.route("/insert_mqtt_feed",methods=['POST'])
# def InsertMqttfeed():

@app.route("/get_user_full_name",methods = ['GET'])
def GetUserFullName():
    user = request.args.get('username')
    cursor = db.cursor(dictionary=True)
    cursor.execute('''
        SELECT first_name, last_name FROM user
        WHERE username = %s
    ''', (user,))
    result = cursor.fetchall()
    resDict = {}
    resDict['result'] = result[0]['first_name'] + " " + result[0]['last_name']
    return resDict

@app.route("/authentication",methods = ['GET'])
def Authentication():
    user = request.args.get('username')
    password = request.args.get('password')
    cursor = db.cursor(dictionary=True)
    cursor.execute('''
        SELECT username FROM user WHERE username = %s AND password = %s
    ''', (user,password))
    result = cursor.fetchall()
    resDict = dict()
    resDict['result']="invalid"
    if result:
        resDict['result']="valid"
    return resDict

@app.route("/get_user_sensors_brief",methods = ['GET'])
def GetUserSensorsBrief():
    cursor = db.cursor(dictionary=True)
    user = request.args.get('username')
    mysqlGetUserSensorBriefQuery = '''
        SELECT S.id, S.sensor_type, ST.value, ST.unit, ST.time
        FROM sensor_stat ST, sensor S
        WHERE
            S.username = %s AND ST.sensor_id = S.id AND (sensor_id,time) IN (
                SELECT sensor_id,MAX(time)
                FROM sensor_stat
                GROUP BY sensor_id
            )
            
    '''

    mysqlGetUserSensorBriefNotRecordedQuery = '''
        SELECT S.id, S.sensor_type
        FROM sensor S
        WHERE S.username = %s AND S.id NOT IN (
            SELECT ST.sensor_id
            FROM sensor_stat ST
        )
    '''
    cursor.execute(mysqlGetUserSensorBriefQuery,(user,))
    resDict = cursor.fetchall()
    for entry in resDict:
        entry['time'] = str(entry['time'])
    cursor.execute(mysqlGetUserSensorBriefNotRecordedQuery,(user,))
    resDict += cursor.fetchall()

    return str(resDict)

@app.route("/get_user_devices_count",methods = ['GET'])
def GetUserSensorCount():
    cursor = db.cursor(dictionary=True)
    user = request.args.get('username')
    resDict = dict()

    # Get the user's number of light sensots
    mysqlGetUserLightSensorCount = '''
        SELECT COUNT(S.id) light_sensors_count
        FROM sensor S
        WHERE S.username = %s AND S.sensor_type = "light"
    '''
    cursor.execute(mysqlGetUserLightSensorCount,(user,))
    resDict["light_sensors_count"] = cursor.fetchall()[0]["light_sensors_count"]

    # Get the user's number of water sensors
    mysqlGetUserWaterSensorCount = '''
        SELECT COUNT(S.id) water_sensors_count
        FROM sensor S
        WHERE S.username = %s AND S.sensor_type = "water"
    '''
    cursor.execute(mysqlGetUserWaterSensorCount,(user,))
    resDict["water_sensors_count"] = cursor.fetchall()[0]["water_sensors_count"]


    # Get the user's number of light actuators
    mysqlGetUserLightActuatorsCount = '''
        SELECT COUNT(cd.id) light_actuators_count
        FROM control_device cd
        WHERE cd.control_device_type = "light" AND cd.sensor_id IN(
            SELECT id
            FROM sensor S
            WHERE sensor_type = "light" AND username=%s
        ) 
    '''
    cursor.execute(mysqlGetUserLightActuatorsCount,(user,))
    resDict["light_actuators_count"] = cursor.fetchall()[0]["light_actuators_count"]

    # Get the user's number of water actuators
    mysqlGetUserWaterActuatorsCount = '''
        SELECT COUNT(cd.id) water_actuators_count
        FROM control_device cd
        WHERE cd.control_device_type = "water" AND cd.sensor_id IN(
            SELECT id
            FROM sensor S
            WHERE sensor_type = "water" AND username=%s
        ) 
    '''
    cursor.execute(mysqlGetUserWaterActuatorsCount,(user,))
    resDict["water_actuators_count"] = cursor.fetchall()[0]["water_actuators_count"]
    return resDict.__str__()


@app.route("/get_user_actuators_brief",methods = ['GET'])
def GetUserActuatorsBrief():
    cursor = db.cursor(dictionary=True)
    user = request.args.get('username')
    mysqlGetUserActuatorsBriefQuery = '''
        SELECT cd.id, cd.name, cd.control_device_type, cd.sensor_id , cd.user_enabled
        FROM control_device CD, sensor S
        WHERE S.username = %s AND CD.sensor_id = S.id
    '''
    cursor.execute(mysqlGetUserActuatorsBriefQuery,(user,))
    resDict = cursor.fetchall()
    return str(resDict)

@app.route("/set_control_device_time",methods=['PUT'])
def SetControlDeviceTime():
    cursor = db.cursor(dictionary=True)
    data = request.json
    deviceId = data['id']
    timeList = data['time_list']
    # Delete all the devices work time to add the new ones in later
    mysqlDeleteControlDeviceTimeQuery = '''
        DELETE FROM control_device_time
        WHERE control_device_id = %s
    '''
    cursor.execute(mysqlDeleteControlDeviceTimeQuery, (deviceId,))
    if len(timeList) == 0:
        return "sucess"
    
    # Insert the new time periods
    mysqlInsertControlDeviceTimeQuery = '''
        INSERT INTO control_device_time (control_device_id,start_time,end_time)
        VALUES
    '''
    for time in timeList:
        mysqlInsertControlDeviceTimeQuery += '(' + str(deviceId) +',\''
        mysqlInsertControlDeviceTimeQuery += time['start_time'] + '\',\''
        mysqlInsertControlDeviceTimeQuery += time['end_time'] +'\'),'
    mysqlInsertControlDeviceTimeQuery = mysqlInsertControlDeviceTimeQuery[:-1]

    cursor.execute(mysqlInsertControlDeviceTimeQuery)
    db.commit()
    print(mysqlInsertControlDeviceTimeQuery)
    return "sucess"

@app.route("/set_control_device_info",methods=['PUT'])
def SetControlDeviceStat():
    return "Not implemented"

@app.route("/set_device_enable",methods=['PUT'])
def SetControlDeviceEnable():
    cursor = db.cursor(dictionary=True)
    dataList = request.json
    if len(dataList) == 0:
        return "no data found"
    deviceEnableList = list()
    deviceDisableList = list()
    for data in dataList:
        if data['enable']:
            deviceEnableList.append(data['id'])
        else:
            deviceDisableList.append(data['id'])

    format_strings = ','.join(['%s'] * len(deviceEnableList))
    cursor.execute('''
        UPDATE control_device
        SET user_enabled = 1
        WHERE id IN (%s)
    ''' % format_strings, tuple(deviceEnableList))


    format_strings = ','.join(['%s'] * len(deviceDisableList))
    cursor.execute('''
        UPDATE control_device
        SET user_enabled = 0
        WHERE id IN (%s)
    ''' % format_strings, tuple(deviceDisableList))
    db.commit()
    return "sucess"



@app.route("/insert_mqtt_feed",methods=['POST'])
def InsertMqttFeed():
    cursor = db.cursor(dictionary=True)
    data = request.json
    owner = data['username']
    username = data['feed_username']
    feed_key = data['password']
    feed_name = data['feed_name']
    paramsTuple = (
        owner,username,feed_key,feed_name
    )
    mysqlInsertMqttFeedQuery =  '''
        INSERT INTO mqtt_feed (owner,username,feed_key,feed_name)
        VALUES (%s,%s,%s,%s)
    '''
    cursor.execute(mysqlInsertMqttFeedQuery,paramsTuple)
    db.commit()
    return "sucess"

@app.route("/insert_sensor",methods=['POST'])
def InsertSensor():
    cursor = db.cursor(dictionary=True)
    data = request.json
    feedUsername = data['feed_username']
    feedName = data['feed_name']
    username = data['username']
    sensorType = data['sensor_type']
    params = (feedUsername,feedName,username,sensorType)
    mysqlInsertSensorQuery = '''
        INSERT INTO sensor (feed_username, feed_name, username, sensor_type)
        VALUES (%s,%s,%s,%s)
    '''
    cursor.execute(mysqlInsertSensorQuery,params)
    db.commit()

    mysqlGetFeedPasswordQuery = '''
        SELECT feed_key
        FROM mqtt_feed
        WHERE username = %s AND feed_name =  %s
    '''
    cursor.execute(mysqlGetFeedPasswordQuery,(username,feedName))
    password = cursor.fetchall()[0]['feed_key']
    mqttParams = dict()
    mqttParams['username'] = feedUsername
    mqttParams['feed_key'] = password
    mqttParams['feed_name'] = feedName
    threading.Thread(target=mqttLoop, args=(mqttParams, )).start()
    return "sucess"

@app.route("/get_user_mqtt_feed_username",methods=['GET'])
def GetUserMqttFeedUsername():
    cursor = db.cursor(dictionary=True)
    user = request.args.get('username')
    mysqlGetUserMqttFeedUsernameQuery = '''
        SELECT DISTINCT username AS feed_username
        FROM mqtt_feed
        WHERE owner = %s
    '''
    cursor.execute(mysqlGetUserMqttFeedUsernameQuery,(user,))
    res = cursor.fetchall()
    return str(res)


@app.route("/get_feed_name_from_username",methods=['GET'])
def GetFeedNameFromUsername():
    cursor = db.cursor(dictionary=True)
    owner = request.args.get('username')
    feedUsername = request.args.get('feed_username')
    params = (owner,feedUsername)
    mysqlGetFeedNameFromUsernameQuery = '''
        SELECT feed_name
        FROM mqtt_feed
        WHERE owner = %s AND username = %s AND (username,feed_name) NOT IN (
            SELECT feed_username,feed_name
            FROM sensor
        ) AND (username,feed_name) NOT IN (
            SELECT feed_username,feed_name
            FROM control_device
        )
    '''
    cursor.execute(mysqlGetFeedNameFromUsernameQuery,params)
    res = cursor.fetchall()
    return str(res)
    
######################

# Fire immediately after the server starts - initializing it
def onServerStart():
    global db
    db = ConnectDatabase()
    RunMqttSensorsListeners()
    threading.Thread(target=SetControlDeviceDisableLoop).start()
    threading.Thread(target=runHttpServer).start()
    #app.run(port=5000)

# Connents to the mysql database and return the connector
def ConnectDatabase():
    return mysql.connector.connect(
        host=mysqlHost,
        user=mysqlUser,
        password=mysqlPassword,
        database=mysqlDatabase,
        auth_plugin='mysql_native_password'
    )
onServerStart()
