DROP SCHEMA IF EXISTS dadn;
CREATE SCHEMA dadn;
USE dadn;

CREATE TABLE user (
    username VARCHAR(255) PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL
);

CREATE TABLE sensor (
    id INT,
    garden_name VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    PRIMARY KEY (id , type)
);

-- relay
CREATE TABLE control_device (
    id INT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    garden_name VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    lowest_stats FLOAT,
    highest_stats FLOAT
);

CREATE TABLE light (
    id INT PRIMARY KEY,
    FOREIGN KEY (id)
        REFERENCES control_device (id)
);

CREATE TABLE faucet (
    id INT PRIMARY KEY,
    FOREIGN KEY (id)
        REFERENCES control_device (id)
);

CREATE TABLE garden (
    mqtt_name VARCHAR(255), -- mqtt username
    username VARCHAR(255),
    mqtt_key VARCHAR(255),
    PRIMARY KEY (mqtt_name),
    FOREIGN KEY (username)
        REFERENCES user (username)
);


CREATE TABLE stats (
	id INT AUTO_INCREMENT,
    sensorID INT,
    timerecorded TIMESTAMP,
    type VARCHAR(255),
    val VARCHAR(255),
    PRIMARY KEY (id),
    FOREIGN KEY (sensorID , type)
        REFERENCES sensor (id , type)
);

ALTER TABLE sensor
ADD CONSTRAINT FOREIGN KEY sensor(username, garden_name) REFERENCES garden(username, mqtt_name);

ALTER TABLE control_device
ADD CONSTRAINT FOREIGN KEY control_device(username, garden_name) REFERENCES garden(username, mqtt_name);

ALTER TABLE control_device
ADD COLUMN sensorID INT;
ALTER TABLE control_device
ADD COLUMN sensortype VARCHAR(255);
ALTER TABLE control_device
ADD CONSTRAINT FOREIGN KEY (sensorID, sensortype) REFERENCES sensor(id, type);