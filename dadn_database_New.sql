-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jun 12, 2021 at 09:43 AM
-- Server version: 5.7.28
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 1;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dadn`
--
DROP SCHEMA IF EXISTS `dadn`;
CREATE DATABASE IF NOT EXISTS `dadn` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dadn`;

-- --------------------------------------------------------

--
-- Table structure for table `control_device`
--

CREATE TABLE IF NOT EXISTS `control_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sensor_id` int(11) NOT NULL,
  `sensor_type` varchar(255) NOT NULL,
  `control_device_type` varchar(255) DEFAULT NULL,
  `lowest_stats` float DEFAULT NULL,
  `highest_stats` float DEFAULT NULL,
  `feed_username` varchar(255) NOT NULL,
  `feed_name` varchar(255) NOT NULL,
  `user_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `feed_username` (`feed_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `control_device_time`
--

CREATE TABLE IF NOT EXISTS `control_device_time` (
  `control_device_id` int(11) NOT NULL,
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `end_time` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`control_device_id`,`start_time`,`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mqtt_feed`
--

CREATE TABLE IF NOT EXISTS `mqtt_feed` (
  `feed_key` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `feed_name` varchar(255) NOT NULL,
  PRIMARY KEY (`username`,`feed_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sensor`
--

CREATE TABLE IF NOT EXISTS `sensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_username` varchar(255) NOT NULL,
  `feed_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `sensor_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`sensor_type`),
  KEY `sensor_ibfk_1` (`username`),
  KEY `sensor_ibfk_2` (`sensor_type`),
  KEY `feed_username_constraint` (`feed_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sensor_stat`
--

CREATE TABLE IF NOT EXISTS `sensor_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(10) DEFAULT NULL,
  `value` varchar(11) NOT NULL,
  `time` timestamp NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `sensor_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor_stat_constraint` (`sensor_id`),
  KEY `sensor_stat_ibfk_2` (`sensor_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `control_device`
--
ALTER TABLE `control_device`
  ADD CONSTRAINT `control_device_ibfk_1` FOREIGN KEY (`sensor_id`, `sensor_type`) REFERENCES `sensor` (`id`, `sensor_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `control_device_ibfk_2` FOREIGN KEY (`feed_username`) REFERENCES `mqtt_feed` (`username`)
  ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `control_device_time`
--
ALTER TABLE `control_device_time`
  ADD CONSTRAINT `device_time_constraint` FOREIGN KEY (`control_device_id`) REFERENCES `control_device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sensor`
--
ALTER TABLE `sensor`
  ADD CONSTRAINT `feed_username_constraint` FOREIGN KEY (`feed_username`) REFERENCES `mqtt_feed` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sensor_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sensor_stat`
--
ALTER TABLE `sensor_stat`
  ADD CONSTRAINT `sensor_stat_constraint` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sensor_stat_ibfk_2` FOREIGN KEY (`sensor_type`) REFERENCES `sensor` (`sensor_type`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
